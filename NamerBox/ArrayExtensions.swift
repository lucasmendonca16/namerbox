//
//  ArrayExtensions.swift
//  NamerBox
//
//  Created by Lucas Mendonça on 4/17/16.
//
//

import Foundation

/** This class extends the array type to include other useful functions it still lacks
 */
extension Array where Element:Equatable {
    
    /** This appends all the objects from an array into another one.
     Usage: arrayA.appendAll(arrayB)
     */
    mutating func appendAll(array: Array<Element>) {
        for element in array {
            self.append(element)
        }
    }
    
    /** This enables removal of objects by object reference only,
     without having to necessarily pass the object's index.
     Usage: array.removeObject(object)
     */
    mutating func removeObject<U: Equatable>(object: U) {
        var index: Int = -1
        for (idx, objectToCompare) in self.enumerate() {
            if let to = objectToCompare as? U {
                if object == to {
                    index = idx
                }
            }
        }; if index != -1 {
            self.removeAtIndex(index)
        }
    }
    
    /** This returns a random element from the Array
     */
    func random() -> Element? {
        if (self.count > 0) {
            return self[Int(arc4random_uniform(UInt32(self.count)))]
        }
        return nil
    }
    
    /** This enables retrieval of the last element of the array.
     Usage: Array.last()
     */
    var last: Element {
        return self[self.endIndex - 1]
    }
    
    /** This removes duplicates or the array elements.
        Usage: Array.removeDuplicates()
    */
    mutating func removeDuplicates(){
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        self = result
    }
}
