//
//  MiscExtensions.swift
//  NamerBox
//
//  Created by Lucas Mendonça on 2/3/16.
//
//

import Foundation
import UIKit

// MARK: - Points Distance

/** This extends the CGPoint type to calculate the distance between two points
Usage: let distance = pointA.distanceTo(pointB)
*/
extension CGPoint {
    func distanceTo(point:CGPoint) -> CGFloat {
        let dx = point.x - self.x
        let dy = point.y - self.y
        return sqrt(CGFloat(dx * dx + dy * dy))
    }
}

// MARK: - Random Number Generator Extensions

/** Extends the Float type to enable random numbers
to be generated with a much visible range.
Usage: Float.random(10, to: 35)
*/
extension Float {
    static func random() -> Float {
        return Float(Float(arc4random()) / Float(UInt32.max))
    }
    static func random(start:Float, to:Float) -> Float {
        assert(start < to)
        return Float.random() * (to - start) + start
    }
}

/** Extends the Int type to enable random numbers
 to be generated with a much visible range.
 Usage: Int.random(10, to: 35)
 */
extension Int {
    static func random(start:Int, to:Int) -> Int {
        return Int(arc4random_uniform(UInt32(to+1)) + UInt32(start))
    }
}

// MARK: - UIView Extensions

extension UIView {
    
    /** Enables removal of all view constraints at once */
    func removeAllConstraints() {
        var list = [NSLayoutConstraint]()
        if(self.superview != nil){
            for c in self.superview!.constraints {
                if c.firstItem as? UIView == self || c.secondItem as? UIView == self {
                    list.append(c)
                }
            }
            self.superview!.removeConstraints(list)
        }
        self.removeConstraints(self.constraints)
        self.translatesAutoresizingMaskIntoConstraints = true
    }
}

/** Optional Extension
 Usage: myOptionalVar.ifNil(defaultValue)
 */

extension Optional {
    public func ifNil(value: Wrapped) -> Wrapped {
        if let v = self {
            return v
        }
        return value
    }
}

// MARK: String Extensions

/** Facilitates localization */
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localizedWithComment(comment: String) -> String {
        return NSLocalizedString(self, comment: comment)
    }
    
    mutating func removeAccentuation() {
        let asciiString:NSData = self.dataUsingEncoding(NSASCIIStringEncoding, allowLossyConversion: true)!
        self = String(data: asciiString, encoding: NSUTF8StringEncoding)!
    }
    
    func syllabsDividedBy(separator: Character) -> String{
        var firstVowelFound : Bool = false
        var indices : [String.CharacterView.Index] = []
        var wordSyllabicDivided : String = self
        
        if (!self.isEmpty){
            for index in self.characters.indices {
                if(!firstVowelFound && self[index].isVowel()){
                    firstVowelFound = true
                } else if(self[index].isVowel() && self[index.predecessor()].isConsonant() && index.predecessor() > self.startIndex){
                    if(self[index.predecessor().predecessor()].isConsonant()){
                        let groupsOfA = self[index.predecessor().predecessor()].getConsonantGroup()
                        let groupsOfB = self[index.predecessor()].getConsonantGroup()
                        if ((groupsOfA.contains(0) && groupsOfB.contains(2)) || (groupsOfA.contains(1) && groupsOfB.contains(3))){
                            indices.append(index.predecessor().predecessor())
                        } else { indices.append(index.predecessor())}
                    } else { indices.append(index.predecessor())}
                } else if(index > self.startIndex){ // a vogal é antecedida por outra vogal
                    if(self[index.predecessor()].isVowel() && self[index].isVowel() && (self[index.predecessor()] == self[index]) ){
                        indices.append(index)
                    }
                }
            }
        }
        for index in indices.reverse() {
            wordSyllabicDivided.insert(separator, atIndex: index)
        }
        return wordSyllabicDivided
    }
    
}



// MARK: Character Identifiers

extension Character {
    var vogais : String { return "AEOIUY" }
    var semivogais : String { return "IU" }
    var consoantes : String { return "BCDFGHJKLMNPQRSTVWXZ" }
    var gruposDeConsoantes : [String]{ return ["BCDFGPTV", "CLN", "LR", "H"] }
    
    func isConsonant() -> Bool{
        for char in consoantes.characters{
            if(self == char) { return true }
        }; return false
    }
    
    func getConsonantGroup() -> [Int]{
        var indices : [Int] = []
        
        for group in gruposDeConsoantes{
            for char in group.characters{
                if(self == char) {
                    indices.append(gruposDeConsoantes.indexOf(group)!)
                }
            }
        }; return indices
    }
    
    func isVowel() -> Bool{
        for char in vogais.characters{
            if(self == char){ return true }
        }; return false
    }
    
    func isSemiVowel() -> Bool{
        for char in semivogais.characters{
            if(self == char){ return true }
        }; return false
    }
}

func ifNil<T>(value:T?, use:T) -> T {
    if let v = value {
        return v
    }; return use
}

/** This function is for concurrency. It prevents that two threads will execute the same code in parallel
 
 Use:
 synced(self) {
 println("This is a synchronized closure")
 }
 */
func synced(lock: AnyObject, closure: () -> ()) {
    objc_sync_enter(lock)
    closure()
    objc_sync_exit(lock)
}
