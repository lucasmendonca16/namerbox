//
//  NavigationController.swift
//  NamerBox
//
//  Created by Lucas Mendonça on 2/3/16.
//
//

import UIKit

/** This is a view controller that manages the navigation of hierarchical content, making it easier for the user to navigate.
 */

class NavigationController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBar()
        setStatusBar()
        setToolbar()
        //self.view.backgroundColor = UIColor.appColor()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    func setStatusBar(){
        // Sets statusBar background color
        let statusBarView = UIView(frame: CGRectMake(0, 0, self.view.frame.width, 20))
        
        // This line sets the statusBar color to a darker shade of the app's theme color
        statusBarView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.20)
//        statusBarView.backgroundColor = UIColor.appColor().colorWithAlphaComponent(0.75)
        statusBarView.layer.zPosition = 10000
        
        self.view.addSubview(statusBarView)
    }
    
    func setNavigationBar(){
        // Sets navigationBar background color
        
        // This line sets the navigationBar color to a darker shade of the app's theme color
        self.navigationBar.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.20)
        
        // This line sets the navigationBar color to transparent
//        self.navigationBar.backgroundColor = UIColor.appColor().colorWithAlphaComponent(0.75)
        
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.translucent = true
        
        // Sets navigationBar button's color
        self.navigationBar.tintColor = UIColor.whiteColor()
        
        // Sets navigationBar title's color
        self.navigationBar.titleTextAttributes = NSDictionary(object: UIColor.whiteColor(), forKey: NSForegroundColorAttributeName) as? [String : AnyObject]
    }
    
    func setToolbar(){
        // Sets translucent Toolbar background color
        //self.toolbar.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(0.22)
        self.toolbar.setBackgroundImage(UIImage(), forToolbarPosition: .Any, barMetrics: UIBarMetrics.Default)
        self.toolbar.barStyle = .Black
        self.toolbar.barTintColor = UIColor.whiteColor()
        self.toolbar.tintColor = UIColor.whiteColor()
        if let items = self.toolbar.items {
            for item in items {
                item.tintColor = UIColor.whiteColor()
            }
        }
        self.toolbar.translucent = true
    }
}
