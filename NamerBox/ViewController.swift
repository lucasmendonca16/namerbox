//
//  ViewController.swift
//  NamerBox
//
//  Created by Lucas Mendonça on 2/2/16.
//
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    
    var relatedWords : [String] = ["VIAGEM", "WANDERLUST", "LUGARES", "BARATO", "NOVO", "DIVERTIDO", "MARCOS", "KOBUCHI"]
    
//    var languageCodes : [String] = ["af", "ach", "ak", "am", "ar", "az", "be", "bem", "bg", "bh", "bn", "br", "bs", "ca", "ckb", "co", "cs", "cy", "da", "de", "el", "en", "eo", "es", "et", "eu", "fa", "fi", "fr", "fy", "ga", "gd", "gl", "gu", "ha", "haw", "hi", "hr", "ht", "hu", "hy", "id", "ig", "is", "it", "iw", "ja", "jw", "ka", "kg", "kk", "km", "kn", "ko", "ku", "ky", "la", "lo", "lt", "lv", "mg", "mi", "mk", "ml", "mn", "mo", "mr", "ms", "mt" , "ne", "nl", "nn", "no", "ny", "nyn", "pa", "pcm", "pl", "ps", "pt-BR", "pt-PT", "qu", "rm", "rn", "ro", "ru", "rw", "sd", "sh", "si", "sk", "sl", "sn", "so", "sq", "sr", "sr-ME", "st", "su", "sv", "sw", "ta", "te", "tg", "th", "ti", "tk", "tl", "tn", "to", "tr", "tt", "tum", "tw", "ug", "uk", "ur", "uz", "vi", "wo", "xh", "yi", "yo", "zh-CN", "zh-TW", "zu"]
    
    var languageCodes : [String] = ["en", "es", "fr", "pt-BR", "pt-PT", "sw", "zh-CN", "zh-TW"]
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var activityIndicatorContainerView: UIView!
    @IBOutlet weak var activityIndicatorContainerViewheightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var textToTranslateField: UITextField!
    @IBOutlet weak var textToTranslateFieldTopConstraint: NSLayoutConstraint!
    
    var translatedArray : [String] = []
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textView: UITextView!
    
    var loading: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.view.backgroundColor = UIColor.appColor()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }
    
    @IBAction func ButtonPressed(sender: UIButton) {
        // self.textView.text = getAllCombinations(relatedWords).joinWithSeparator("\n")
        
        self.textView.text = getSyllabicDivisionOf(relatedWords).joinWithSeparator("\n")
    }
    
    func getSyllabicDivisionOf(words: [String]) -> [String]{
        var wordsSyllabicDivided : [String] = []
        
        for word in words{
            wordsSyllabicDivided.append(word.syllabsDividedBy("."))
        }
        return wordsSyllabicDivided
    }
    
    func getAllCombinations(words: [String]) -> [String] {
        let result = Array(permute(words)).sort()
        return result
    }
    
    func permute(list: [String], minLength: Int = 2) -> Set<String> {
        func permute(fromList: [String], toList: [String], minLength: Int, inout set: Set<String>) {
            if toList.count >= minLength {
                set.insert(toList.joinWithSeparator(""))
            }
            if !fromList.isEmpty {
                for (index, item) in fromList.enumerate() {
                    var newFrom = fromList
                    newFrom.removeAtIndex(index)
                    permute(newFrom, toList: toList + [item], minLength: minLength, set: &set)
                }
            }
        }
        var set = Set<String>()
        permute(list, toList:[], minLength: minLength, set: &set)
        return set
    }
    
    @IBAction func translate(sender: UIButton) {
        startTranslating()
    }
    
    func startTranslating(){
        var translationCount = 0
        var textToTranslate: String = (self.textToTranslateField.text!).lowercaseString
        var translatedText : String = ""
        let source = "pt-br"
        
        if(!self.loading && !textToTranslate.isEmpty){
            self.loading = true
            
            textToTranslate.removeAccentuation()
            
            self.updateLoadingAnimationTo(true)
            
            for target in self.languageCodes {
                Alamofire.request(.GET, "https://www.googleapis.com/language/translate/v2?parameters", parameters: ["key":"AIzaSyD0hxZEnd0RKl0M8XO7i0so8dGX_9n8Wf8","q":"\(textToTranslate)",
                    "source":source,"target":target])
                    .responseJSON { response in
                        
                        guard let jsonObject = response.result.value else{
                            print("Error! JSON response is not valid")
                            return
                        }
                        
                        let json = JSON(jsonObject)
                        
                        if let s = json["data"]["translations"][0]["translatedText"].string {
                            translatedText = s.lowercaseString
                            translatedText.removeAccentuation()
                            self.translatedArray.append(translatedText)
                            print("\ta")
                        } else {
                            print(json["data"]["translations"][0]["translatedText"].error ?? "")
                            print("\tb")
                        }
                        
                    }.responseData(completionHandler: { (response) in
                        translationCount += 1
                        //print("Loading: ... \(translationCount) / \(self.languageCodes.count)")
                        if(translationCount == self.languageCodes.count){
                            self.translationDone(textToTranslate)
                            self.updateLoadingAnimationTo(false)
                            self.loading = false
                        }
                    })
            }
        } else if(textToTranslate.isEmpty){
            self.textView.text = ""
        }
    }
    
    func translationDone(textToTranslate: String){
        var input:String = textToTranslate.lowercaseString
        
        input.removeAccentuation()
        self.translatedArray.removeObject(input)
        self.translatedArray.sortInPlace()
        self.translatedArray.removeDuplicates()
        
        for string in translatedArray {
            if (string.containsString("?") || string.isEmpty){
                self.translatedArray.removeObject(string)
            }
        }
        
        var allTranslatedWords : String = "Traduções de \"\(input)\":\n\n"
        for string in translatedArray{
            allTranslatedWords += string + "\n"
        }
        
        self.textView.text = allTranslatedWords
        allTranslatedWords.removeAll()
        translatedArray.removeAll()
    }
    
    func updateLoadingAnimationTo(loading: Bool){
        if(loading){
            self.activityIndicatorContainerViewheightConstraint.constant = 40
            self.textToTranslateFieldTopConstraint.constant = 0
        } else {
            self.activityIndicatorView.stopAnimating()
            self.activityIndicatorContainerViewheightConstraint.constant = 0
            self.textToTranslateFieldTopConstraint.constant = 8
        }
        
        UIView.animateWithDuration(0.2, animations: {
            self.view.layoutIfNeeded()
        }) { (true) in
            if(loading){
                self.activityIndicatorView.startAnimating()
            }
        }
    }
    
    @IBAction func returnPressed(sender: AnyObject) {
        startTranslating()
    }
    
}
